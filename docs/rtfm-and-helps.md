# RTFM and Helps in Unix Shell

In Unix Shell, the access to help or manual is very simply. and we have two way for then.

## Manual

Just following this command :

```shell
man <command>
```

## Helps

For most commande line applications, this one is developed with help. It is therefore possible to call it as follows :

```shell
<command> -h / --help
```
