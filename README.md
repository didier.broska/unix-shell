# Bash tools

This repos groups all articles or tools about Unix Shell as Bash, Zsh or FishShell.

## RTFM and Helps

In Unix Shell, when you post a issue about a simple command it reply RTFM. So I write an article about it, so got [here](./docs/rtfm-and-helps.md)
